/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tarealistas;
import java.util.*;
/**
 *
 * @author Roman Franco
 */
public class TareaListas {
public static Scanner sc=new Scanner(System.in);
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        String Nom,Ape,Fec;
        ArrayList <Estudiante> estudiantes=new ArrayList();
        for(int k=1;k<=20;k++){
            System.out.print("Digite el Nombre: ");
            Nom=sc.next();
            System.out.print("Digite el Apellido: ");
            Ape=sc.next();
            System.out.print("Digite la Fecha de Nacimiento: ");
            Fec=sc.next();
            estudiantes.add(new Estudiante(Nom, Ape, Fec));     
                    
        }
        
        System.out.println("Lista antes de borrar");
        for(Estudiante est: estudiantes){
           System.out.println(est.getNombre() + " " + est.getApellido()+ " "+ est.getFecha());
        }
        
        System.out.print("Digite letras a buscar: ");
        String letras;
        letras=sc.next();
        
        for (int i = 0; i < estudiantes.size(); i++) {
            if(estudiantes.get(i).getNombre().contains(letras)){               
               estudiantes.remove(i);               
            }
        }
        
        System.out.println("Lista despues de borrar");
        for(Estudiante est: estudiantes){
           System.out.println(est.getNombre() + " " + est.getApellido()+ " "+ est.getFecha());
        }
    


        
        
    }
    
}
