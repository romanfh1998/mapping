package javaapplication5;


import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class mapping {

    public static void main(String[] args) throws Exception {
        String url = "https://mx.answers.yahoo.com/question/index?qid=20110617153112AA110kC";
        Document document = Jsoup.connect(url).get();

        String text = document.select("div").first().text();
        System.out.println(text);

        Elements links = document.select("a");
        for (Element link : links) {
            System.out.println(link.attr("href"));
        }
    }

}