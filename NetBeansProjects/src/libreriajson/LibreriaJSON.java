/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package libreriajson;
import org.json.*;
import java.net.*;
import java.io.*;
import org.springframework.boot.*;

/**
 *
 * @author Roman Franco
 */
public class LibreriaJSON {

    /**1
     * @param args the command line arguments
     */
    public static void main(String[] args) throws JSONException {
        
        try {
		      // Indicamos la URL donde nos conectamos
		      URL url = new URL("http://www.lineadecodigo.com");

		      // Buffer con los datos recibidos
		      BufferedReader in = null;
		   
		      try {
		        // Volcamos lo recibido al buffer
		        in = new BufferedReader(new InputStreamReader(
		           url.openStream()));
		      } catch(Throwable t){}

		      // Transformamos el contenido del buffer a texto
		      String inputLine;
		      String inputText="";
                      
                      
		      // Mientras haya cosas en el buffer las volcamos a las
		      // cadenas de texto 
		      while ((inputLine = in.readLine()) != null)
		      {
		        inputText = inputText + inputLine;
		      }
//                        JSONObject obj = new JSONObject(inputText);
                       
		      // Mostramos el contenido y cerramos la entrada
		      System.out.println("El contenido de la URL es: " + inputText);
		      in.close();
		   
		    } catch (MalformedURLException me) {
		      System.out.println("URL erronea");
		    } catch (IOException ioe) {
		      System.out.println("Error IO");
        }
        
        
    }
}
